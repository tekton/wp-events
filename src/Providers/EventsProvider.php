<?php namespace Tekton\Wordpress\Events\Providers;

use Tekton\Support\ServiceProvider;
use Tekton\Wordpress\Events\EventManager;

class EventsProvider extends ServiceProvider {

    function register() {
        $this->app->singleton('wp.events', function() {
            return new EventManager();
        });
    }

    function boot() {
        $this->setupPostType();
        $this->setupMeta();

        add_filter('automatic_post_objects', function($post_hijacks) {
            return array_merge($post_hijacks, [
                'events' => \Tekton\Wordpress\Events\Event::class,
            ]);;
        });

        app('blade')->directive('events', function ($expression) {
            $output = '<?php $query = Events::query('.$expression.') ?>';
            $output .= '<?php while($query->have_posts()) : $event = \\__post($query); ?>';

            return $output;
        });

        app('blade')->directive('endevents', function () {
            return "<?php endwhile; \\wp_reset_postdata(); ?>";
        });
    }

    function setupPostType() {
        add_action('init', function() {
        	register_post_type( 'events', array(
        		'labels' => array(
        			'name' => __( 'Events' ),
        			'singular_name' => __( 'Event' )
        		),
        		'public' => true,
        		'has_archive' => true,
        		'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
        	));
        });
    }

    function setupMeta() {
        add_action( 'metabox_init', function() {
            $event_info = create_metabox( array(
                'id'            => 'event_info',
                'title'         => __( 'Event Info', 'tekton-wp-events' ),
                'object_types'  => array( 'events'), // Post type
                'context'       => 'normal',
                'priority'      => 'default',
                'show_names'    => true, // Show field names on the left
                // 'closed'     => true, // Keep the metabox closed by default
            ));

            $start_date = $event_info->add_field( array(
                'name'         => __( 'Start Date', 'tekton-wp-events' ),
                'id'   => meta_key('event', 'start_date'),
                'type' => 'text_date',
                'date_format' => DATE_ISO,
                'desc' => 'Start date of the event.',
            ));
            $end_date = $event_info->add_field( array(
                'name'         => __( 'End Date', 'tekton-wp-events' ),
                'id'   => meta_key('event', 'end_date'),
                'type' => 'text_date',
                'date_format' => DATE_ISO,
                'desc' => 'End date of the event. If it\'s a one day event you set it the same as the start date. Do not leave it blank.',
            ));
            $registration_link = $event_info->add_field(array(
                'id' => meta_key('event', 'registration_link'),
                'name' => __('Link to Registration Form', 'tekton-wp-events'),
                'type' => 'text_url',
                'desc' => 'If there is no registration required then this field can be left blank. Otherwise, input the full URL to the form page (e.g. a Google form).',
            ));
            $promo_id = $event_info->add_field(array(
                'id' => meta_key('event', 'promo'),
                'name' => __('Promo Video ID', 'tekton-wp-events'),
                'type' => 'text_medium',
                'desc' => 'If the URL to the video is "https://www.youtube.com/watch?v=dQw4w9WgXcQ" then "dQw4w9WgXcQ" is the ID.',
            ));

            if (is_admin_edit()){
                $grid = create_grid($event_info);
                $row = $grid->addRow();
                $row->addColumns(array($start_date, $end_date));

                $row = $grid->addRow();
                $row->addColumns(array($registration_link));

                $row = $grid->addRow();
                $row->addColumns(array($promo_id));
            }

            $event_details = create_metabox( array(
                'id'            => 'event_details',
                'title'         => __( 'Event Details', 'tekton-wp-events' ),
                'object_types'  => array( 'events'), // Post type
                'context'       => 'normal',
                'priority'      => 'default',
                'show_names'    => true, // Show field names on the left
                // 'closed'     => true, // Keep the metabox closed by default
            ));
            $group_field_id = $event_details->add_field( array(
                'id'          => meta_key('event', 'details'),
                'type'        => 'group',
                'description' => __( 'Specify event details that will be shown in the summary box on the event page', 'tekton-wp-events' ),
                // 'repeatable'  => false, // use false if you want non-repeatable group
                'options'     => array(
                    'group_title'   => __( 'Detail {#}', 'tekton-wp-events' ), // since version 1.1.4, {#} gets replaced by row number
                    'add_button'    => __( 'Add Another Entry', 'tekton-wp-events' ),
                    'remove_button' => __( 'Remove Entry', 'tekton-wp-events' ),
                    'sortable'      => true, // beta
                    // 'closed'     => true, // true to have the groups closed by default
                ),
            ));

                // Id's for group's fields only need to be unique for the group. Prefix is not needed.
            $label = $event_details->add_group_field($group_field_id, array(
                'name' => 'Label',
                'id'   => 'label',
                'type' => 'text_small',
                'before_row' => '', // Without this CMB2-grid errors
                'after_row' => '', // Without this CMB2-grid errors
            ));

            $content = $event_details->add_group_field($group_field_id, array(
                'name' => 'Content',
                'id'   => 'content',
                'type' => 'text',
                'before_row' => '', // Without this CMB2-grid errors
                'after_row' => '', // Without this CMB2-grid errors
            ));

            if (is_admin_edit()){
                //Create a default grid
                $grid = create_grid($event_details);

                //Create now a Grid of group fields
                $group_grid = create_group_grid($grid, $group_field_id);
                $row = $group_grid->addRow();
                $row->addColumns(array($label, $content));
            }
        });
    }
}
