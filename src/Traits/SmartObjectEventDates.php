<?php namespace Tekton\Wordpress\Events\Traits;

use DateTime;

trait SmartObjectEventDates {

    function is_multi_day() {
        return ($this->dates->end != $this->dates->start) ? true : false;
    }

    function is_single_day() {
        return ! $this->is_multi_day();
    }

    function is_running() {
        $today = new DateTime();

        if ($today >= $this->dates->start && $today <= $this->dates->end) {
            return true;
        }

        return false;
    }

    function is_past() {
        $today = new DateTime();

        if ($this->dates->end < $today) {
            return true;
        }

        return false;
    }

    function is_upcoming() {
        return ! $this->is_past();
    }
}
