<?php namespace Tekton\Wordpress\Events\Facades;

class Events extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'wp.events'; }
}
