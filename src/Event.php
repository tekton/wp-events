<?php namespace Tekton\Wordpress\Events;

class Event extends \Tekton\Wordpress\Post implements \Tekton\Wordpress\Events\EventDatesInterface {

    use \Tekton\Wordpress\Events\Traits\SmartObjectEventDates;

    function get_property($key) {
        $start = null;

        switch ($key) {
            case 'dates': return $this->dates();
            case 'podcast': return $this->podcast();
            case 'details': return post_meta('event', 'details', $this->id);
            case 'promo': return post_meta('event', 'promo', $this->id);
            case 'registration_link': return post_meta('event', 'registration_link', $this->id);
        }

        return parent::get_property($key);
    }

    protected function dates() {
        $dates = (object) array(
            'start' => make_datetime(post_meta('event', 'start_date', $this->id), DATE_ISO),
            'end' =>  post_meta('event', 'end_date', $this->id),
        );

        if (empty($dates->start)) {
            return null;
        }

        $dates->end = ( ! empty($dates->end)) ? make_datetime($dates->end, DATE_ISO) : $dates->start;
        return $dates;
    }

    protected function podcast() {
        $podcast = (object) array(
            'series' => post_meta('event', 'podcast_series', $this->id),
            'episode' => post_meta('event', 'podcast_episode', $this->id),
            'url' => '',
        );

        $podcast->url = ( ! empty($podcast->episode)) ? get_permalink($podcast->episode) : '';
        $podcast->url = ( ! empty($podcast->series)) ? get_term_link($podcast->series, 'series') : $podcast->url;
        return $podcast;
    }

    function has_podcast() {
        return ( ! empty($this->podcast->url)) ? true : false;
    }
}
