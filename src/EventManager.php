<?php namespace Tekton\Wordpress\Events;

use WP_Query;

class EventManager {

    protected $meta = [];

    const ALL_EVENTS = 0;
    const UPCOMING_EVENTS = 1;
    const PAST_EVENTS = 2;
    const RUNNING_EVENTS = 3;
    const FUTURE_EVENTS = 4;

    function __construct() {
        $this->meta[self::ALL_EVENTS] = [];

        // End Date >= Today
        $this->meta[self::UPCOMING_EVENTS] = [
            [
                'key' => meta_key('event', 'end_date'),
                'value' => date(DATE_ISO),
                'compare' => '>=',
                'type' => 'DATE'
            ]
        ];

        // End Date >= Today && Start Date <= Today
        $this->meta[self::RUNNING_EVENTS] = ['relation' => 'AND',
            [
                'key' => meta_key('event', 'start_date'),
                'value' => date(DATE_ISO),
                'compare' => '<=',
                'type' => 'DATE'
            ],
            [
                'key' => meta_key('event', 'end_date'),
                'value' => date(DATE_ISO),
                'compare' => '>=',
                'type' => 'DATE'
            ]
        ];

        // End Date < Today
        $this->meta[self::PAST_EVENTS] = [
            [
                'key' => meta_key('event', 'end_date'),
                'value' => date(DATE_ISO),
                'compare' => '<',
                'type' => 'DATE'
            ]
        ];

        // Start Date > Today
        $this->meta[self::FUTURE_EVENTS] = [
            [
                'key' => meta_key('event', 'start_date'),
                'value' => date(DATE_ISO),
                'compare' => '>',
                'type' => 'DATE'
            ],
        ];
    }

    function query($status = 0, array $args = []) {
        if (is_array($status)) {
            $args = $status;
            $status = 0;
        }
        else {
            $status = $this->normalizeStatus($status);
        }

        $queryArgs = array_merge([
            'post_type' => 'events',
            'meta_query' => $this->meta[$status],
            'post_status' => 'publish',
            'posts_per_page' => -1,

            // Default, order by start date
            'order' => 'ASC',
            'orderby' => 'meta_value', // We want to organize the events by date
            'meta_key' => meta_key('event', 'start_date'),
        ], $args);

        return new WP_Query($queryArgs);
    }

    function normalizeStatus($key) {
        if (is_string($key)) {
            switch (strtolower($key)) {
                case 'past': return self::PAST_EVENTS;
                case 'upcoming': return self::UPCOMING_EVENTS;
                case 'running': return self::RUNNING_EVENTS;
                case 'future': return self::FUTURE_EVENTS;
                default: return self::ALL_EVENTS;
            }
        }
        else {
            switch ($key) {
                case self::PAST_EVENTS: return self::PAST_EVENTS;
                case self::UPCOMING_EVENTS: return self::UPCOMING_EVENTS;
                case self::RUNNING_EVENTS: return self::RUNNING_EVENTS;
                case self::FUTURE_EVENTS: return self::FUTURE_EVENTS;
                default: return self::ALL_EVENTS;
            }
        }
    }
}
