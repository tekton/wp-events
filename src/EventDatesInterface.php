<?php namespace Tekton\Wordpress\Events;

interface EventDatesInterface {
    function is_multi_day();
    function is_single_day();
    function is_running();
    function is_past();
    function is_upcoming();
}
